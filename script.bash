#!/bin/bash

###### GLOBALS ############################################################
size=(0 0) # stores the screen size for whiptail
i=0        # used for indexing the array
###########################################################################

function setup {
  # check if stty is installed on system
  loc=$(whereis stty | awk '{print $2}')

  # if stty is installed, use returned values as screen size
  if [[ "$loc" == "/bin/stty" ]]; then
    for n in $(stty size); do
      size[i]=$n
      i=$((i+1));
    done
    rows=${size[0]} # sets number of rows
    cols=${size[1]} # sets number of columns
  else # failed to locate stty, use default values
    rows=25
    cols=200
  fi
}

function gauge {
	load=0	
	{
		while [ $load -le 100 ]; do
			sleep .05
			load=$((load+5))
			echo $load
		done
	} | whiptail --gauge "Calculating..." $rows $cols 0
}

function interrupts {
  s=$( grep intr /proc/stat | awk '{print $2}' )
	gauge
  e=$( grep intr /proc/stat | awk '{print $2}' )
  inters=$((e-s))
  msg="There were $inters interrupts..."
}

function procsAndThreads {
	ts=$( ps -eT | awk '{a[$1]} END {for (i in a) c+=1} END {print c}' )
	ps=$( ps e | awk '{a[$1]} END {for (i in a) c+=1} END {print c}' )
	msg="Processes: $ps\nThreads: $ts"
}

function ctxtswitches {
	load=0
	s=$( grep ctxt /proc/stat | awk '{print $2}' )
	gauge
	e=$( grep ctxt /proc/stat | awk '{print $2}' )
	res=$((e-s))
	msg="There were $res context switches..."
}

function kernelAndUser {
	load=0
	usr=$(grep -E 'cpu[0-9]+' /proc/stat | awk ' {a[$2]} END {for (i in a) sum += i} END {print sum} ')
	krn=$(grep -E 'cpu[0-9]+' /proc/stat | awk ' {a[$4]} END {for (i in a) sum += i} END {print sum} ')
	gauge
	usr_e=$(grep -E 'cpu[0-9]+' /proc/stat | awk ' {a[$2]} END {for (i in a) sum += i} END {print sum} ')
	krn_e=$(grep -E 'cpu[0-9]+' /proc/stat | awk ' {a[$4]} END {for (i in a) sum += i} END {print sum} ')
	usr_tot=$((usr_e - usr))
	krn_tot=$((krn_e - krn))
	msg="Time in user mode: $usr_tot jiffies\nTime in kernel mode: $krn_tot jiffies"
}


setup

while true; do
  choice=$(whiptail --title "Oblig 1, oppgave 1" --menu "Velg et alternativ" $rows $cols 16 \
    "1" "Who am I, and what's the name of this script?" \
    "2" "Time since last cold boot" \
    "3" "How many processes and threads are currently running" \
		"4" "How many context swicthes occured the last second" \
    "5" "Time running in usermode and kernelmode the last second" \
    "6" "How many interrupts occured the last second" \
    "9" "Exit the application" 3>&2 2>&1 1>&3 # swaps stdin, stdout, stderr
    )

  case $choice in
    "1")
      msg="I am $( whoami ) and the name of this script is $0" ;;
    "2")
      msg="$( uptime -p )" ;;
		"3")
			procsAndThreads ;;
		"4")
			ctxtswitches ;;
    "5")
      kernelAndUser ;;
    "6")
      interrupts ;;
    "9")
      exit ;;
  esac
  whiptail --msgbox "$msg" $rows $cols
done

exit
