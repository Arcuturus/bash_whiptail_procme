#!/bin/bash

function genFile {
	# remove existing file
	for file in $PID*.meminfo; do
		if [ -f "$file" ]; then
			rm "$file"
		else
			continue
		fi
	done
	date_time=$( date '+-%d%m%Y-%H%M%S' ) # date with format
	ext=".meminfo" # file extension
	file=$PID$date_time$ext # generate file name
	# print everything into file
	{
		printf "*********** Memory info for process PID %d ***********\n" "$PID"
		printf "Total use of virtual memory (VmSize): %d kb\n" "$VmSize"
		printf "Total private virtual memory (VmData+VmStk+VmExe): %d kb\n" "$VmPriv"
		printf "Total shared virtual memory (VmLib): %d kb\n" "$VmLib"
		printf "Total use of physical memory (VmRSS): %d kb\n" "$VmRSS"
		printf "Physical memory used for page table (VmPTE): %d kb\n\n" "$VmPTE"
	} >> "$file"	
}

function getInfo {
	#file=$( /proc/$PID/status )
	VmSize=$( grep VmSize /proc/"$PID"/status | awk '{print $2}' )
	VmPriv=$( grep -E 'VmData|VmStk|VmExe' /proc/"$PID"/status | awk '{sum += $2} END {print sum}' )
	VmLib=$( grep VmLib /proc/"$PID"/status | awk '{print $2}' )
	VmRSS=$( grep VmRSS /proc/"$PID"/status | awk '{print $2}' )
	VmPTE=$( grep VmPTE /proc/"$PID"/status | awk '{print $2}' )
	genFile VmSize VmPriv VmLib VmRSS VmPTE
}

if [ "$#" -gt "0" ]; then
	for PID in "$@"; do
		getInfo PID
	done
else
	printf "To use: %s <pid1> <pid2> ... <pidn>\n" "$0"
fi

