#!/bin/bash

# pgrep firefox

# ps --no-headers -o maj_flt pgrep firefox

printf "As I do not have chrome installed on my system, this script can also take a process as parameter.\n"
printf "To run this script on another process, just pass it as a parameter next time!\n\n"


if [ $# -lt 1 ]; then
	if [[ $(pgrep chrome) ]]; then
		for pid in $( pgrep chrome ); do
			printf "Chrome %d har forårsaket " "$pid"
			flt=$( ps --no-headers -o maj_flt "$pid" )
			printf " %d major page faults!" "$flt"
			if [ "$flt" -gt 1000 ]; then
				printf " (mer enn 1000)!\n"
			else
				printf "\n"
			fi
		done
	else
		printf "Could not find Chrome! Make sure it is installed and running\n"
	fi
else
	if [[ $(pgrep "$1") ]]; then
		for pid in $( pgrep -x "$1" ); do
			printf "%s %d har forårsaket " "$1" "$pid"
			flt=$(ps --no-headers -o maj_flt "$pid")
			printf " %d major page faults!" "$pid"
			if [ "$flt" -gt 1000 ]; then
				printf " (mer enn 1000)!\n"
			else
				printf "\n"
			fi
		done
	else
		printf "Could not locate %s, please try again\n" "$1"
		printf "To use: %s <pname>\n" "$0"
	fi
fi

