#!/bin/bash

if [ "$#" -ne 1 ]; then
	printf "Command takes one argument! You passed %d!\n" "$#"
	printf "To use: %s </path/to/dir> \n" "$0"
	exit 1
fi

size=0

partition=$(df "$1" | awk '{print $5}' | sed -n 2p)
no_files=$( find "$1" -type f | wc -l )
largest_name=$(find "$1" -type f -exec du -Sh {} + | sort -rh | head -1 | awk '{print $2}')
largest_size=$(find "$1" -type f -exec du -Sh {} + | sort -rh | head -1 | awk '{print $1}')

for file in $(find "$1" -type f -printf "%s\n" | sort -rn); do
	size=$((size + file))
done

average=$((size / no_files))	

hard_name=$( find "$1" -xdev -printf "%p\n" | sort -rn | head -1 )
hard_count=$( find "$1" -xdev -printf "%n\n" | sort -rn | head -1 )

printf "Partisjonen %s befinner seg på er %s full\n" "$1" "$partition"
printf "Det finnes %d filer\n" "$no_files"
printf "Den største er %s som er %s \n" "$largest_name" "$largest_size"
printf "Gjennomsnittlig filstørrelse er ca %d bytes\n" "$average"
printf "Filen %s har flest hardlinks, den har %d\n" "$hard_name" "$hard_count"
