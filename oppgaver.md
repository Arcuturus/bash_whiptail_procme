# Oblig 2

Husk å ta med en README fil som beskriver hvordan du har sikret kodekvaliteten.
F.eks. med `shellcheck` som visstnok er en greie.

For å gi `.bash` filen execute-permissions kan du kjøre `chmod +x filnavn`

# Oppgave 7.5)
## a) (OBLIG) Prosesser og tråader.

Lag et script myprocinfo.bash som gir brukeren følgende meny med tilhørende
funksjonalitet:

```
 1 - Hvem er jeg og hva er navnet på dette scriptet?
 2 - Hvor lenge er det siden siste boot?
 3 - Hvor mange prosesser og tr˚ader finnes?
 4 - Hvor mange context switch'er fant sted siste sekund?
 5 - Hvor stor andel av CPU-tiden ble benyttet i kernelmode
    og i usermode siste sekund?
 6 - Hvor mange interrupts fant sted siste sekund?
 9 - Avslutt dette scriptet

Velg en funksjon:
```

Hint: bruk en switch case for a håndtere menyen, og hvert menyvalg bør føre til
en echo hvor svaret settes inn via $(), og for punkt 4-6 kan du hente ut info fra
/proc/stat, se http://www.linuxhowtos.org/System/procstat.htm

## b) (FRIVILLIG)
Skriv om myprocinfo.bash scriptet til a bruke å whiptail til a vise meny. Hint:
man whiptail og
http://stackoverflow.com/questions/1562666/bash-scripts-whiptail-file-select
https://en.wikibooks.org/wiki/Bash_Shell_Scripting/Whiptail

---

# Oppgave 8.6)
## c) (OBLIG) Page Faults
Page faults Anta at nettleseren chrome kjører. Skriv et bash script chromemajPF.bash
som skriver ut antall major page faults hver av chrome sine prosesser har forarsaket.
I tillegg skal scriptet skrive ut en melding om en av chrome sine prosesser har
forarsaket mer enn 1000 major page faults. Du kan finne antall major page faults
for en PID med

```
$ ps --no-headers -o maj_flt 4468
113
```

og du kan finne chrome sine PIDs med

```
$ pgrep chrome
4377
4390
4468
4479
Scriptet ditt bør oppføre seg slik:
$ ./chromemajPF.bash
Chrome 4377 har forårsaket 1160 major page faults (mer enn 1000!)
Chrome 4390 har for˚arsaket 41 major page faults
Chrome 4468 har for˚arsaket 113 major page faults
Chrome 4479 har for˚arsaket 31 major page faults
```
---

## d) (OBLIG) En prosess sin bruk av virtuelt og fysisk minne.

Skriv et script procmi.bash som tar et sett med prosess-ID’er som kommandolinjeargument
og for hver av disse prosessene skriver til en fil PID-dato.meminfo
følgende info:

 (a) Total bruk av virtuelt minne (V mSize)
 (b) Mengde virtuelt minne som er privat (V mData + V mStk + V mExe)
 (c) Mengde virtuelt minne som er shared (V mLib)
 (d) Total bruk av fysisk minne (V mRSS)
 (e) Mengde fysisk minne som benyttes til page table (V mP T E, som da gjerne
    er multi-level, slik at det vil variere mye fra prosess til prosess)

Hint: man proc og les om filen `/proc/[pid]/status`, samt se i filen `/proc/meminfo`.

Eksempel kjøring:
```
$ bash procmi.bash 1 1985 17579
$ ls 17579*
17579-20100216-15:26:31.meminfo
$ cat 17579-20100216-15\:26\:31.meminfo
******** Minne info om prosess med PID 17579 ********
Total bruk av virtuelt minne (VmSize): 3444KB
Mengde privat virtuelt minne (VmData+VmStk+VmExe): 516KB
Mengde shared virtuelt minne (VmLib): 1596KB
Total bruk av fysisk minne (VmRSS): 1112KB
Mengde fysisk minne som benyttes til page table (VmPTE): 32KB
```

# Oppgave 9.4)
## a) (OBLIG) Informasjon om deler av filsystemet

Skriv et script fsinfo.bash som tar en directory som argument og skriver ut
 *  Hvor stor del av partisjonen directorien befinner seg pa som er full
 *  Hvor mange filer finnes i directorien (inkl subdirectorier), gjennomsnittlig
    filstørrelse, og full path til den største filen
 *  Hvilken fil (IKKE ta med directories) har flest hardlinker til seg selv

Eksempel kjøring:
```
$ bash fsinfo.bash /opsys/
Partisjonen /opsys/ befinner seg på er 91% full
Det finnes 1137 filer.
Den største er /opsys/00-X/Jan2007.doc som er 14110208 (14M) stor.
Gjennomsnittlig filstørrelse er ca 186208 bytes.
Filen /opsys/06-filesystems/lab/c har flest hardlinks, den har 3.
```
